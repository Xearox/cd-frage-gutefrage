import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CD {
	
	private HashMap<Integer, Titel> titelAufCD = new HashMap<Integer, Titel>();
	
	private String name;
	
	public CD() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer anzahlTitelAufCD() {
		return titelAufCD.size() + 1;
	}
	
	public void addTitel(int index, Titel titel) {
		titelAufCD.put(index, titel);
	}
	
	public ArrayList<Titel> gibAlleTitelOhneIndex() {
		return new ArrayList<Titel>(titelAufCD.values());
	}
	
	public List<Titel> gibAlleTitelNachNameSortiert(boolean absteigend) {
		return titelAufCD
				.values()
				.stream()
				.sorted((t1,t2) -> {
					return (absteigend ? t2.getName().compareTo(t1.getName()) : t1.getName().compareTo(t2.getName()));
				})
				.collect(Collectors.toList());
	}
	
	public HashMap<Integer, Titel> gibTitelAufCD() {
		return titelAufCD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
