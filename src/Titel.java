
public class Titel {
	
	/**
	 * Der Name des Titels
	 */
	private String name;
	
	/**
	 * Der K�nstler des Titels
	 */
	private String interpret;
	
	/**
	 * Die Inhalt des Titels - Pseudoinhalt, hier w�rden die Daten der MP3 drin sein
	 */
	private String content;
	
	/**
	 * Leerer Konstruktor
	 */
	public Titel() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Konstruktor zum erstellen eines Titels
	 * @param name
	 * @param interpret
	 * @param content
	 */
	public Titel(String name, String interpret, String content) {
		this.name = name;
		this.interpret = interpret;
		this.content = content;
	}
	
	public String getName() {
		return name;
	}
	public String getInterpret() {
		return interpret;
	}
	public String getContent() {
		return content;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setInterpret(String interpret) {
		this.interpret = interpret;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	

}
